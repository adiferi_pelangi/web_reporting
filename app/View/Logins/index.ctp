      <!-- Content area -->
      <div class="content d-flex justify-content-center align-items-center">

        <?php
        echo $this->Form->create('login', array('action' => 'LoginMe'));
        ?>

        <!-- Login form -->
        <div class="card wmin-md-350">
          <div class="card-body">
            <div class="text-center mb-3">
              <div class="form-group">
                <label>User ID / Nomer HP</label>
                <!-- flashmessage -->
                <?=$this->Session->flash(); ?>
                <?php echo $this->Form->input('username', array('label' => false, 'size' => 20, 'div' => false, 'class' => 'form-control', 'placeholder' => 'input User ID / Nomer HP')); ?>
              </div>

              <div class="form-group">
                <label>password</label>
                <?php echo $this->Form->input('password', array('label' => false, 'size' => 20, 'div' => false, 'class' => 'form-control', 'placeholder' => 'input password')); ?>
              </div>


              <!-- auth google -->
              <div>
                <div class="g-recaptcha" data-sitekey="<?php echo Configure::read('Recaptcha.SiteKey'); ?>">
                </div>
                <?php echo $this->Html->script('https://www.google.com/recaptcha/api.js"'); ?>
              </div>


              <div class="form-group">
                <?php
                // echo $this->Form->end('Login',array('class'=>'btn btn-primary btn-block'));
                ?>

                <button type="submit" class="btn btn-primary btn-block">Log in <i class="icon-circle-right2 ml-2"></i></button>
              </div>
            </div>
          </div>
          <!-- /login form -->

        </div>
        <!-- /content area -->
      </div>
      <!-- /main content -->