<?php echo $this->Html->docType('html5'); ?> 
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="SIMPHONY ONLINE POINT BANK" />
	<meta name="keywords" content="SIMPHONY ONLINE POINT BANK"" />
	<meta name="robots" content="noindex,nofollow" />
	<link href="<?php echo $this->Html->url("/img/favicon.ico") ?>" rel="icon" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <title>Payment Point Online Bank - <?php echo $title_for_layout;?></title>

    <?php 
    echo $this->Html->css('limitles/icomoon/styles');
    echo $this->Html->css('limitles/bootstrap.min');
    echo $this->Html->css('limitles/bootstrap_limitless.min');
    echo $this->Html->css('limitles/colors.min');
    echo $this->Html->css('limitles/components.min');
    echo $this->Html->css('limitles/layout.min');

    echo $this->Html->script('limitles/main/jquery.min');
    echo $this->Html->script('limitles/main/bootstrap.bundle.min');
    echo $this->Html->script('limitles/plugins/loaders/blockui.min');
    echo $this->Html->script('limitles/plugins/ui/slinky.min');

    echo $this->Html->script('limitles/plugins/visualization/d3/d3.min');
    echo $this->Html->script('limitles/plugins/visualization/d3/d3_tooltip');
    echo $this->Html->script('limitles/plugins/forms/styling/switchery.min');
    echo $this->Html->script('limitles/plugins/forms/selects/bootstrap_multiselect');
    echo $this->Html->script('limitles/plugins/ui/moment/moment.min');
    echo $this->Html->script('limitles/plugins/pickers/daterangepicker');
    echo $this->Html->script('limitles/plugins/collap/jquery.aCollapTable.min');
    echo $this->Html->script('limitles/demo_pages/dashboard');
    echo $this->Html->script('limitles/demo_pages/form_select2');
    echo $this->Html->script('limitles/plugins/tables/datatables/datatables.min');
    echo $this->Html->script('limitles/demo_pages/datatables_basic');
    echo $this->Html->script('limitles/plugins/notifications/sweet_alert.min');
    ?> 
</head>

<body style='margin:0px;padding:0px;'>
    <!-- nav -->
    <div class="navbar navbar-expand-md navbar-dark">
        <div class="navbar-brand wmin-0 mr-5">
            <a href="#" class="d-inline-block">
                <h3>S</h3>
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav">
            </ul>

            <span class="badge bg-success-400 badge-pill ml-md-3 mr-md-auto">Selamat Datang <?php echo $this->Session->read('Auth.User.first_name'); ?>  <label id="infosaldo"></label></span>
            <ul class="navbar-nav">

                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                        <span>Acount</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="<?php echo $this->webroot; ?>Users/logout" class="dropdown-item"><i class="icon-switch2"></i>Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- end nav -->
    <div class="navbar navbar-expand-md navbar-light">
        <div class="text-center d-md-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-navigation">
                Menu
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-navigation">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a href="#" onclick='$("li").removeClass("current_page_item");' class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-file-stats mr-2"></i>
                        Laporan
                    </a>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item" onclick="trxandmutasi()">Transaksi & Mutasi</a>
                        <a href="#" class="dropdown-item" onclick="manual_advice()">Manual Advice</a>
                        <a href="#" class="dropdown-item" onclick="rekaptransaksi()">Rekap Transaksi</a>
                    </div>
                </li>

                <?php if ($this->Session->read("Auth.User.group_id")!=12) { ?>
                <li class="nav-item dropdown">
                    <a href="#" onclick='$("li").removeClass("current_page_item");' class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-copy2 mr-2"></i>
                        Cetak Ulang
                    </a>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item" onclick="ppob()">PPOB</a>
                        <a href="#" class="dropdown-item" onclick="tiketkai()">Tiket Kai</a>
                        <?php if (strpos($this->Session->read("Auth.User.username"),"plg")!==FALSE && ($this->Session->read("Auth.User.group_id")!=3 && $this->Session->read("Auth.User.group_id")!=8 )  ) { ?>
                        <a href="#" class="dropdown-item" onclick="tiketkai()">eror</a>
                    </div>
                </li>
                <?php }else{ ?>
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-magic-wand mr-2"></i>
                        Ganti Pin
                    </a>
                </li> 
                <?php }} ?>

                <li class="nav-item">
                    <a href="#" class="navbar-nav-link" onclick="gantipassword()">
                        <i class="icon-key mr-2"></i>
                        Ganti Password
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-train2 mr-2"></i>
                        KAI
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" onclick='$("li").removeClass("current_page_item");' class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-menu2 mr-2"></i>
                        Lainya
                    </a>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item" onclick="chekharga()">Chek Harga</a>
                        <a href="#" class="dropdown-item" onclick="Lregistrasi()">Registrasi</a>
                        <a href="#" class="dropdown-item" onclick="requesttiketdeposit()">Request Tiket Deposit</a>
                        <a href="#" class="dropdown-item" onclick="tiketkomplain()">Tiket Komplain</a>
                        <a href="#" class="dropdown-item" onclick="kemitraan()">Kemitraan</a>
                        <a href="#" class="dropdown-item" onclick="syaratketentuan()">Syarat dan Ketentuan</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="page-content pt-0">
        <div id="wrapper" class="content-wrapper">
            <!-- Page Content -->
            <div id="page" class="content">
                <!-- Content Area -->
                <div id="content" class="12u">

                    <section id="sectionContent" style="width:100%; min-height:500px;">
                        <?php $msg=$this->Session->flash(); ?>
                        <?php $msg_auth = $this->Session->flash('auth'); ?>
                        <?php if (trim($msg)!="" || trim($msg_auth)!= "" ) { ?>
                        <div class="ui-state-error ui-corner-all" style="padding: .7em .7em;">
                           <?php echo $msg; ?>
                           <?php echo $msg_auth; ?>
                       </div>
                       <?php } ?>
                       <?php echo $this->fetch('content'); ?>
                   </section>
               </div>
           </div>
           <!-- Page Content -->
       </div>
   </div>
   <!-- Wrapper Ends Here -->

   <div class="navbar navbar-expand-lg navbar-light px-0">
    <div class="container">
        <div class="text-center d-lg-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                <i class="icon-unfold mr-2"></i>
                Footer
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-footer">
            <span class="navbar-text">
                © <?php echo date(Y); ?> <a href="http://www.pelangi.co.id">PT PELANGI INDODATA</a>
            </span>

            <ul class="navbar-nav ml-lg-auto">
            </ul>
        </div>
    </div>
</div>

<canvas id="hidden_screenshot" style="display: none;" />
</body>
</html>