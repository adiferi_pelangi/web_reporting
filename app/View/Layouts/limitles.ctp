<?php echo $this->Html->docType('html5'); ?> 
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="PPOB" />
	<meta name="keywords" content="PPOB" />
	<meta name="robots" content="noindex,nofollow" />
	<link href="<?php echo $this->Html->url("/img/favicon.ico") ?>" rel="icon" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <title>Payment Point Online Bank - <?php echo $title_for_layout;?></title>

    <?php 
    echo $this->Html->css('limitles/icomoon/styles');
    echo $this->Html->css('limitles/bootstrap.min');
    echo $this->Html->css('limitles/bootstrap_limitless.min');
    echo $this->Html->css('limitles/colors.min');
    echo $this->Html->css('limitles/components.min');
    echo $this->Html->css('limitles/layout.min');

    // echo $this->Html->script('limitles/app');
    // echo $this->Html->script('limitles/costum');

    echo $this->Html->script('limitles/main/jquery.min');
    echo $this->Html->script('limitles/main/bootstrap.bundle.min');
    echo $this->Html->script('limitles/plugins/loaders/blockui.min');
    echo $this->Html->script('limitles/plugins/ui/slinky.min');

    echo $this->Html->script('limitles/plugins/visualization/d3/d3.min');
    echo $this->Html->script('limitles/plugins/visualization/d3/d3_tooltip');
    echo $this->Html->script('limitles/plugins/forms/styling/switchery.min');
    echo $this->Html->script('limitles/plugins/forms/selects/bootstrap_multiselect');
    echo $this->Html->script('limitles/plugins/ui/moment/moment.min');
    echo $this->Html->script('limitles/plugins/pickers/daterangepicker');
    echo $this->Html->script('limitles/plugins/collap/jquery.aCollapTable.min');
    echo $this->Html->script('limitles/demo_pages/dashboard');
    echo $this->Html->script('limitles/demo_pages/form_select2');
    echo $this->Html->script('limitles/plugins/tables/datatables/datatables.min');
    echo $this->Html->script('limitles/demo_pages/datatables_basic');
    echo $this->Html->script('limitles/plugins/notifications/sweet_alert.min');
    ?>

    <?php
	//css
    // echo $this->Html->css('styles');    
    // echo $this->Html->css('table-style');
 //    echo $this->Html->css('themes/base/jquery.ui.all');
	// //js
 //    echo $this->Html->script('jquery-1.9.1');
 //    echo $this->Html->script('jquery-1.10.2.min');
 //    echo $this->Html->script('mega/5grid/jquery');
 //    echo $this->Html->script('mega/5grid/init.js?use=desktop&amp;mobileUI=0&amp;mobileUI.theme=none');
 //    echo $this->Html->script('jquery-ui-1.10.3/ui/jquery.ui.effect');
 //    echo $this->Html->script('jquery-ui-1.10.3/ui/jquery.ui.effect-clip');
 //    echo $this->Html->script('jquery-ui-1.10.3/ui/jquery.ui.core');
 //    echo $this->Html->script('jquery-ui-1.10.3/ui/jquery.ui.widget');
 //    echo $this->Html->script('jquery-ui-1.10.3/ui/jquery.ui.datepicker');
 //    echo $this->Html->script('jquery.print');
 //    echo $this->Html->script('stdlib');
 //    echo $this->Html->script('numeral/min/numeral.min');
    // echo $this->Html->script('adi/bootstrap/js/bootstrap');
    ?>

    <style type="text/css" media="print">
    @media print {
       .page-break{ display:block; page-break-before:always; }
   }
   .coret{
    text-decoration: none;
}

.centerFlex {
  align-items: center;
  display: flex;
  justify-content: center;
}

</style>
</head>

<body style='margin:0px;padding:0px;'>
    <!-- nav -->
    <div class="navbar navbar-expand-md navbar-dark bg-success-400">
        <div class="navbar-brand wmin-0 mr-5">
            <a href="#" class="d-inline-block">
                <?php echo $this->Html->image("logoo_pelangi.png",array('width'=>'100')); ?>
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a href="<?php echo $this->webroot; ?>" class="btn btn-warning"><i class="icon-bag " ></i> Transaksi</a>
                    <a href="<?php echo $this->webroot; ?>transactions/administrasi" class="btn btn-warning"><i class="icon-user"></i> Administrasi</a>
                </li>
            </ul>

            <span class="badge bg-success-400 badge-pill ml-md-3 mr-md-auto">Selamat Datang <?php echo $this->Session->read('Auth.User.first_name'); ?>  <label id="infosaldo"></label></span>
            <ul class="navbar-nav">

                <li class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                        <span>Acount</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="<?php echo $this->webroot; ?>Users/logout" class="dropdown-item"><i class="icon-switch2"></i>Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- end nav -->
    
    <div class="navbar navbar-expand-md navbar-light">
        <div class="text-center d-md-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-navigation">
                Menu
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-navigation">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a href="#" onclick='$("li").removeClass("current_page_item");' class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-file-stats mr-2"></i>
                        Laporan
                    </a>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item" onclick="trxandmutasi()">Transaksi & Mutasi</a>
                        <a href="#" class="dropdown-item" onclick="manual_advice()">Manual Advice</a>
                        <a href="#" class="dropdown-item" onclick="rekaptransaksi()">Rekap Transaksi</a>
                    </div>
                </li>

                <?php if ($this->Session->read("Auth.User.group_id")!=12) { ?>
                <li class="nav-item dropdown">
                    <a href="#" onclick='$("li").removeClass("current_page_item");' class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-copy2 mr-2"></i>
                        Cetak Ulang
                    </a>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item" onclick="ppob()">PPOB</a>
                        <a href="#" class="dropdown-item" onclick="tiketkai()">Tiket Kai</a>
                        <?php if (strpos($this->Session->read("Auth.User.username"),"plg")!==FALSE && ($this->Session->read("Auth.User.group_id")!=3 && $this->Session->read("Auth.User.group_id")!=8 )  ) { ?>
                        <a href="#" class="dropdown-item" onclick="tiketkai()">eror</a>
                    </div>
                </li>
                <?php }else{ ?>
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-magic-wand mr-2"></i>
                        Ganti Pin
                    </a>
                </li> 
                <?php }} ?>

                <li class="nav-item">
                    <a href="#" class="navbar-nav-link" onclick="gantipassword()">
                        <i class="icon-key mr-2"></i>
                        Ganti Password
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link">
                        <i class="icon-train2 mr-2"></i>
                        KAI
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" onclick='$("li").removeClass("current_page_item");' class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-menu2 mr-2"></i>
                        Lainya
                    </a>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item" onclick="chekharga()">Chek Harga</a>
                        <a href="#" class="dropdown-item" onclick="Lregistrasi()">Registrasi</a>
                        <a href="#" class="dropdown-item" onclick="requesttiketdeposit()">Request Tiket Deposit</a>
                        <a href="#" class="dropdown-item" onclick="tiketkomplain()">Tiket Komplain</a>
                        <a href="#" class="dropdown-item" onclick="kemitraan()">Kemitraan</a>
                        <a href="#" class="dropdown-item" onclick="syaratketentuan()">Syarat dan Ketentuan</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="page-content pt-0">
        <div id="wrapper" class="content-wrapper">
            <!-- Page Content -->
            <div id="page" class="content">
                <!-- Content Area -->
                <div id="content" class="12u">

                    <section id="sectionContent" style="width:100%; min-height:500px;">
                        <?php $msg=$this->Session->flash(); ?>
                        <?php $msg_auth = $this->Session->flash('auth'); ?>
                        <?php if (trim($msg)!="" || trim($msg_auth)!= "" ) { ?>
                        <div class="ui-state-error ui-corner-all" style="padding: .7em .7em;">
                           <?php echo $msg; ?>
                           <?php echo $msg_auth; ?>
                       </div>
                       <?php } ?>
                       <?php echo $this->fetch('content'); ?>
                   </section>
               </div>
           </div>
           <!-- Page Content -->
       </div>
   </div>
   <!-- Wrapper Ends Here -->

   <div class="navbar navbar-expand-lg navbar-light px-0">
    <div class="container">
        <div class="text-center d-lg-none w-100">
            <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                <i class="icon-unfold mr-2"></i>
                Footer
            </button>
        </div>

        <div class="navbar-collapse collapse" id="navbar-footer">
            <span class="navbar-text">
                © <?php echo date(Y); ?> <a href="http://www.pelangi.co.id">PT PELANGI INDODATA</a>
            </span>

            <ul class="navbar-nav ml-lg-auto">
            </ul>
        </div>
    </div>
</div>

<canvas id="hidden_screenshot" style="display: none;" />
</body>
</html>

<script>
$(document).ready(function() {

    $("#notifSection").html('<?php echo $this->Html->image('loading.gif'); ?>');
            //alert($("input:radio[name ='radTipe']:checked").val());
            $.get('<?= $this->Html->url("/Posts/index") ?>',
                function(response, status, xhr) {
                    if (status == 'success') {
                        $("#notifSection").html(response);
                    }
                });
            $('#button_logout').on({
                mouseover: function(){
                    $(this).attr("src","../img/button_logout_blue.png");
                },
                mouseleave: function(){
                    $(this).attr("src","../img/button_logout.png");
                },
                click: function(){
                    $(this).attr("src","../img/button_logout_blue_pressed.png");
                }
            });

            $('#button_transaksi').on({
                mouseover: function(){
                    $(this).attr("src","../img/button_transaksi_blue.png");
                },
                mouseleave: function(){
                    $(this).attr("src","../img/button_transaksi.png");
                },
                click: function(){
                    $(this).attr("src","../img/button_transaksi_blue_pressed.png");
                }
            });

            $('#button_administrasi').on({
                mouseover: function(){
                    $(this).attr("src","../img/button_Administrasi_blue.png");
                },
                mouseleave: function(){
                    $(this).attr("src","../img/button_Administrasi.png");
                },
                click: function(){
                    $(this).attr("src","../img/button_Administrasi_blue_pressed.png");
                }
            });

        });

$(function() {
    $( "#txtTanggalStart" ).datepicker();
    $( "#txtTanggalEnd" ).datepicker();    
    $.get("<?= $this->Html->url("/Users/getCurrentBalance") ?>",
        function(response, status, xhr) {
            if (status == 'success') {
                $("#tdSaldo").html(response);
            }
        }
        );
    $("#txtTanggalStart" ).datepicker({dateFormat:"dd/mm/yy"}).datepicker("setDate",new Date());
    $("#txtTanggalEnd" ).datepicker({dateFormat:"dd/mm/yy"}).datepicker("setDate",new Date());

    $("#cmdCetak").click(function(){
      $("#result").print();
  });
});
</script>

<script type="text/javascript">

$.get("<?= $this->Html->url("/Users/getCurrentBalance") ?>",
    function(response, status, xhr) {
        if (status == 'success') {
            $("#infosaldo").html(response);
        }
    });

function trxandmutasi(){
    $("li").removeClass("current_page_item");
    $("#TrxMutasi").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Transactions/report") ?>",
        function(response, status, xhr) {
            if (status == "success") {
                $("#sectionContent").html(response);
            }
        }
        );
}

function manual_advice(){
    $("li").removeClass("current_page_item");
    $("#TrxMutasi").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Transactions/manualadvice") ?>",
        function(response, status, xhr) {
            if (status == "success") {
                $("#sectionContent").html(response);
            }
        }
        );
}

function rekaptransaksi(){
    $("li").removeClass("current_page_item");
    $("#RekapTrx").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Transactions/downline_report") ?>",
        function(response, status, xhr) {
            if (status == "success") {
                $("#sectionContent").html(response);
            }
        }
        );
}

function ppob(){
    $("li").removeClass("current_page_item");
    $("#reprintMenu").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Transactions/reprint") ?>",
        function(response, status, xhr) {
            if (status == "success") {
                $("#sectionContent").html(response);
            }
        }
        );
}

function tiketkai(){
    $("li").removeClass("current_page_item");
    $("#reprintMenu").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Transactions/reprint_KAI") ?>",
        function(response, status, xhr) {
            if (status == "success") {
                $("#sectionContent").html(response);
            }
        }
        );
}

function gantipin(){
    $("li").removeClass("current_page_item");
    $("#pinMenu").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Users/gantiPin") ?>",
     function(response, status, xhr) {
        if (status == "success") {
           $("#sectionContent").html(response);
       }
   });
}

function gantipassword(){
    $("li").removeClass("current_page_item");
    $("#passwordMenu").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Users/gantiPassword") ?>",
     function(response, status, xhr) {
        if (status == "success") {
           $("#sectionContent").html(response);
       }
   });
}

function kai(){
    $("li").removeClass("current_page_item");
    $("#passwordMenu").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Transactions/kai_index") ?>",
     function(response, status, xhr) {
        if (status == "success") {
           $("#sectionContent").html(response);
       }
   }); 
}

function chekharga(){
    $("li").removeClass("current_page_item");
    $("#PriceList").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Users/priceList") ?>",
        function(response, status, xhr) {
            if (status == "success") {
                $("#sectionContent").html(response);
            }
        });
}

function Lregistrasi(){
    $("li").removeClass("current_page_item");
    $("#Registrasi").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Registers/upload") ?>",
       function(response, status, xhr) {
        if (status == "success") {
            $("#sectionContent").html(response);
        }    
    });
}

function requesttiketdeposit(){
    $("li").removeClass("current_page_item");
    $("#Registrasi").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Deposits/requestTiket") ?>",
        function(response, status, xhr) {
          if (status == "success") {
             $("#sectionContent").html(response);
         }
     });
}

function tiketkomplain(){
    $("li").removeClass("current_page_item");
    $("#ticketKomplain").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Tickets/index") ?>",
        function(response, status, xhr) {
            if (status == "success") {
                $("#sectionContent").html(response);
            }
        });
}

function kemitraan(){
    $("li").removeClass("current_page_item");
    $("#kemitraan").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Mutations/DisplayKirsal") ?>",
       function(response, status, xhr) {
        if (status == "success") {
            $("#sectionContent").html(response);
        }
    });
}

function bayarbanyak(){
    $("li").removeClass("current_page_item");
    $("#kolektif").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Kolektifs") ?>",
        function(response, status, xhr) {
            if (status == "success") {
                $("#sectionContent").html(response);
            }
        });
}

function syaratketentuan(){
    $("li").removeClass("current_page_item");
    $("#agreement").addClass("current_page_item");
    $("#sectionContent").html("<h2><i class='icon-spinner9 spinner'></i></h2>");
    $.get("<?= $this->Html->url("/Users/SyaratKetentuan") ?>",
        function(response, status, xhr) {
            if (status == "success") {
                $("#sectionContent").html(response);
            }
        });
}

function button_logout(){
    document.location='/web_trx_new/Users/logout';
    return false;
}

</script>