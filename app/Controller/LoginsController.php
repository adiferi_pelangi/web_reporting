<?php
class LoginsController extends AppController {

    public function index() {
        if($this->Session->read('status')==1){
            $this->redirect('/users/dashboard');
        }else{
            echo "session null";
        }
        $this->layout = "design_login";
    }

    public function LoginMe() {
        $this->layout = 'ajax'; 
        $this->render(false);
    
        // filter input
        if ($this->request->is('post')) {
            // pr($this->request->data);
            $username = filter_var($this->request->data['login']['username'], FILTER_SANITIZE_STRING);
            $password = filter_var($this->request->data['login']['password'], FILTER_SANITIZE_STRING);
        }

        if($this->__checkRecaptchaResponse($this->request->data['g-recaptcha-response'])){
            //chek otoritas
            $obj = (new AppController())->loginRest($username,$password);
            if(!empty($obj['data']['loginStatus'])){
                $this->Session->write($obj['data']['loginStatus']);
                    if($this->Session->read('status')==1){
                        $this->Flash->set('Login berhasil');
                    }else{
                        // $this->Flash->set($this->Session->read('desc'));
                        $this->Flash->set('Login gagal');
                        $this->redirect('/logins');
                    }
            }else{
                $this->redirect('/logins');
            }

        } else {
            $this->Flash->set('Chapta anda salah');
            $this->redirect('/logins');
        }
    }


    private function __checkRecaptchaResponse($response){
        // verifying the response is done through a request to this URL
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        // The API request has three parameters (last one is optional)
        $data = array('secret' => Configure::read('Recaptcha.SecretKey'),
                        'response' => $response,
                        'remoteip' => $_SERVER['REMOTE_ADDR']);
    
        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data),
            ),
        );
    
        // We could also use curl to send the API request
        $context  = stream_context_create($options);
        $json_result = file_get_contents($url, false, $context);
        $result = json_decode($json_result);
        return $result->success;
    }
}   