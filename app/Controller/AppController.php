<?php
App::uses('Controller', 'Controller');
class AppController extends Controller {

    public function loginRest($username,$password){
        if(($username==""||($password==""))){
            return "input kosong";
        }else{
            $apiUrl = Configure::read('Api.Key');
            $curl = curl_init($apiUrl);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_URL, $apiUrl);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
              'Authorization: '.'PELANGIREST username='.$username.'&password='.$password
            ));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);
            $obj = json_decode($response,true);
            return $obj;
        }
    }


    public function loginRests($username,$password){
        if(($username==""||($password==""))){
            return "input kosong";
        }else{
            $apiUrl = Configure::read('Api.Key');
            $curl = curl_init($apiUrl);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_URL, $apiUrl);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
              'Authorization: '.'PELANGIREST username='.$username.'&password='.$password
            ));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);
            $obj = json_decode($response,true);
            return $obj;
        }
    }

}
